'use strict';

//SECRET NUMBER
const sN = function () {
  let dice = Math.trunc(Math.random() * 20) + 1;
  return dice;
};
let secretNumber = sN();

//SCORE
let score = 20;

//HIGHSCORE
let highscore = 0;

const displayMessage = function (message) {
  document.querySelector('.message').textContent = message;
};

document.querySelector('.check').addEventListener('click', function () {
  const guess = Number(document.querySelector('.guess').value);

  if (!guess) {
    displayMessage('No number!😫');

    //When player wins
  } else if (guess === secretNumber) {
    displayMessage('Correct Number!✌');
    document.querySelector('.number').textContent = secretNumber;
    document.querySelector('body').style.backgroundColor = '#60b347';
    document.querySelector('.number').style.width = '30rem';

    if (score > highscore) {
      highscore = score;
      document.querySelector('.highscore').textContent = highscore;
    }

    //When player is incorrect
  } else if (guess !== secretNumber) {
    if (score > 1) {
      displayMessage(guess > secretNumber ? 'Too high! 😐' : 'Too low! 😐');
      score--;
      document.querySelector('.score').textContent = score;
    } else {
      displayMessage('You lost the game 😥');
      document.querySelector('.score').textContent = 0;
    }
  }
});

//AGAIN BTN
document.querySelector('.again').addEventListener('click', function () {
  secretNumber = sN();
  score = 20;
  displayMessage('Start guessing...');
  document.querySelector('.score').textContent = score;
  document.querySelector('.guess').value = '';
  document.querySelector('body').style.backgroundColor = '#222';
  document.querySelector('.number').value = secretNumber;
  document.querySelector('.number').style.width = '15rem';
  document.querySelector('.number').textContent = '?';
});
